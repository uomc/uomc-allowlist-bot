import { clean, fail, logger } from "./logger.js";
import { RejectReasons } from "./rejectReasons.js";

/** @param {import("discord.js").StringSelectMenuInteraction} interaction */
export async function onSelectSubmit(interaction) {
  const fields = interaction.message.content
    .split("\n")
    .map((x) => x.split(": "));
  const userId = fields[0][1].replaceAll(/[^\d]/g, "");

  logger.info(
    `Received rejection reason "${interaction.values[0]}" for user "${userId}"`
  );

  const user = interaction.message.guild?.members.cache.get(userId);

  if (!user) {
    return fail(
      interaction,
      `Failed to get user "${userId}"`,
      "Failed to get user"
    );
  }

  const rejectReason = RejectReasons[interaction.values.at(0) ?? "other"];

  user
    .send(
      `You were not added to the allowlist on the UoM Minecraft Society server due to an issue with the details you entered.\nThe issue was: **${rejectReason}**.\nPlease correct this and send the allowlist command again, or contact a committee member if you need help.`
    )
    .then(() => {
      logger.info(
        `Sent rejection message to "${userId}" with reason "${rejectReason}"`
      );
      interaction
        .update({
          content:
            clean(interaction) +
            "```diff\n- Rejected - " +
            rejectReason +
            "```",
          components: [],
        })
        .catch(() => logger.error("Failed to mark as rejected"));
    })
    .catch(() =>
      fail(
        interaction,
        `Failed to send rejection message to "${userId}" with reason "${rejectReason}"`,
        "Failed to send rejection message"
      )
    );
}
