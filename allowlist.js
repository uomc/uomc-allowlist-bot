import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  SlashCommandBuilder,
} from "discord.js";
import { logger } from "./logger.js";

const { INFO_CHANNEL, LOG_CHANNEL, RULES_CHANNEL } = process.env;

if (!(INFO_CHANNEL && LOG_CHANNEL && RULES_CHANNEL)) {
  throw new Error("Failed to load environment variables");
}

const Subcommands = {
  WithId: "with-id",
  WithEmail: "with-email",
};

const Options = {
  MinecraftUsername: "minecraft-username",
  SuId: "student-union-id",
  Email: "email",
  IsBedrock: "playing-bedrock"
};

const data = new SlashCommandBuilder()
  .setName("allowlist")
  .setDescription("create allowlist request")
  .addSubcommand((subcommand) =>
    subcommand
      .setName(Subcommands.WithId)
      .setDescription(
        "create allowlist request using your student or SU associate member id number"
      )
      .addStringOption((option) =>
        option
          .setName(Options.MinecraftUsername)
          .setDescription("your minecraft username")
          .setRequired(true)
      )
      .addIntegerOption((option) =>
        option
          .setName(Options.SuId)
          .setDescription("your student or SU associate member id number")
          .setRequired(true)
          .setMinValue(0)
      )
      .addBooleanOption((option) => 
        option
          .setName(Options.IsBedrock)
          .setDescription("Are you a BEDROCK player?")
          .setRequired(false)
      )
  )
  .addSubcommand((subcommand) =>
    subcommand
      .setName(Subcommands.WithEmail)
      .setDescription(
        "create allowlist request using your manchester.ac.uk email address"
      )
      .addStringOption((option) =>
        option
          .setName(Options.MinecraftUsername)
          .setDescription("your minecraft username")
          .setRequired(true)
      )
      .addStringOption((option) =>
        option
          .setName(Options.Email)
          .setDescription("your manchester.ac.uk email address")
          .setRequired(true)
      )
      .addBooleanOption((option) => 
        option
          .setName(Options.IsBedrock)
          .setDescription("Are you a BEDROCK player?")
          .setRequired(false)
      )
  );

const acceptButton = new ButtonBuilder()
  .setCustomId("accept")
  .setLabel("Accept")
  .setStyle(ButtonStyle.Success);

const rejectButton = new ButtonBuilder()
  .setCustomId("reject")
  .setLabel("Reject")
  .setStyle(ButtonStyle.Danger);

const actionRow = new ActionRowBuilder().addComponents(
  acceptButton,
  rejectButton
);

/**
 * @param {import("discord.js").ChatInputCommandInteraction} interaction
 */
const execute = async (interaction) => {
  const minecraftUsername = interaction.options.getString(
    Options.MinecraftUsername
  );

  let isEmail = false;
  let idOrEmail;

  if (interaction.options.getSubcommand() === Subcommands.WithId) {
    idOrEmail = interaction.options.getInteger(Options.SuId).toString();
  } else if (interaction.options.getSubcommand() === Subcommands.WithEmail) {
    isEmail = true;
    idOrEmail = interaction.options.getString(Options.Email);
  }
  let isBedrockPlayer = interaction.options.getBoolean(Options.IsBedrock) ?? false;

  logger.info(
    `Received request for user "${interaction.user}", id/email: "${idOrEmail}", username: "${minecraftUsername}"`
  );

  const channel = interaction.client.channels.cache.get(LOG_CHANNEL);

  if (!channel || !channel.isTextBased()) {
    return logger.error("Failed to get log channel");
  }

  logger.info(isBedrockPlayer)
  const msgRes = await channel
    .send({
      content: `Discord user: ${
        interaction.user
      }\nUsername: ${minecraftUsername}\nStudent ${
        isEmail ? "email" : "ID"
      }: ${idOrEmail}\nIs a bedrock user: ${isBedrockPlayer}`,
      // @ts-expect-error Cannot type actionRow properly with JSDoc. See https://gitlab.com/uomc/uomc-allowlist-bot/-/issues/5
      components: [actionRow],
    })
    .catch(() => false);

  if (msgRes === false) {
    return logger.error(
      `Failed to send request message for user "${interaction.user}", id/email: "${idOrEmail}, username: "${minecraftUsername}`
    );
  }

  const rulesChannel = interaction.client.channels.cache.get(RULES_CHANNEL);
  const infoChannel = interaction.client.channels.cache.get(INFO_CHANNEL);

  await interaction
    .reply({
      content: `Thanks, that's all! You'll get the 'allowlisted' role on discord when we've added you.\nPlease go read the ${rulesChannel} and ${infoChannel} channels in the meantime to make sure you're up to date!`,
      ephemeral: true,
    })
    .catch(() => {
      logger.error(
        `Failed to send request receipt for user "${interaction.user}", id/email: "${idOrEmail}, username: "${minecraftUsername}`
      );
      msgRes !== true &&
        msgRes.edit({
          content: msgRes.content + "\n**Failed to send receipt**",
        });
    });
};

export default { data, execute };
