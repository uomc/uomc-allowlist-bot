import { Client, Events, GatewayIntentBits, REST, Routes } from "discord.js";
import "dotenv/config";
import command from "./allowlist.js";
import { onButtonClick } from "./buttonInteraction.js";
import { logger } from "./logger.js";
import { onSelectSubmit } from "./stringSelectMenuInteraction.js";

const { TOKEN, CLIENT_ID, GUILD_ID } = process.env;

if (!(TOKEN && CLIENT_ID && GUILD_ID)) {
  logger.fatal("Failed to load environment variables");
  throw new Error("Failed to load environment variables");
}

// Refresh commands

const rest = new REST().setToken(TOKEN);

(async () => {
  try {
    logger.info("Started refreshing application (/) commands");

    await rest.put(Routes.applicationGuildCommands(CLIENT_ID, GUILD_ID), {
      body: [command.data.toJSON()],
    });

    logger.info("Successfully reloaded application (/) commands");
  } catch (error) {
    logger.error({ error }, "Failed to reload application (/) commands");
  }
})();

// Create event handlers

const client = new Client({ intents: [GatewayIntentBits.Guilds] });

client.once(Events.ClientReady, () => {
  logger.info(`Logged in as ${client.user?.tag}`);
});

client.on(Events.InteractionCreate, async (interaction) => {
  if (interaction.isChatInputCommand()) {
    logger.debug({ interaction }, "Received command interaction");
    command.execute(interaction);
  } else if (interaction.isButton()) {
    logger.debug({ interaction }, "Received button interaction");
    onButtonClick(interaction);
  } else if (interaction.isStringSelectMenu()) {
    logger.debug({ interaction }, "Received select menu interaction");
    onSelectSubmit(interaction);
  }
});

client.login(TOKEN);
