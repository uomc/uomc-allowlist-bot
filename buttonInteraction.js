import {
  ActionRowBuilder,
  StringSelectMenuBuilder,
  StringSelectMenuOptionBuilder,
} from "discord.js";
import "dotenv/config";
import { clean, fail, logger } from "./logger.js";
import { RejectReasons } from "./rejectReasons.js";

const { ALLOWLISTED_ROLE, BEDROCK_ROLE, CONSOLE_CHANNEL, INFO_CHANNEL } = process.env;

/** @param {import("discord.js").ButtonInteraction} interaction */
export async function onButtonClick(interaction) {
  if (!(ALLOWLISTED_ROLE && BEDROCK_ROLE && CONSOLE_CHANNEL && INFO_CHANNEL)) {
    return logger.fatal("Failed to load environment variables");
  }

  const fields = interaction.message.content
    .split("\n")
    .map((x) => x.split(": "));
  const userId = fields[0][1].replaceAll(/[^\d]/g, "");
  const minecraftUsername = fields[1][1];
  const useBedrock = fields[3][1] == "true" ? true : false;
  logger.info(
    `Received "${interaction.customId}" button click for user "${userId}"`
  );

  const user = await interaction.message.guild?.members.fetch(userId);

  if (!user) {
    return fail(
      interaction,
      `Failed to get user "${userId}"`,
      "Failed to get user"
    );
  }

  if (interaction.customId === "accept") {
    const channel = interaction.client.channels.cache.get(CONSOLE_CHANNEL);

    
    if (channel?.isTextBased()) {
      let success = true;

      if (useBedrock) {
        success = await channel
          .send(`fwhitelist add ${minecraftUsername}`)
          .then(() =>
            logger.info(`Added "${minecraftUsername}" (bedrock) to the allowlist`)
          )
          .catch(() => false);
      } else {
        success = await channel
          .send(`whitelist add ${minecraftUsername}`)
          .then(() =>
            logger.info(`Added "${minecraftUsername}" to the allowlist`)
          )
          .catch(() => false);
      }


      if (success === false) {
        return fail(
          interaction,
          `Failed to add "${minecraftUsername}" to the allowlist`,
          `Failed to add to allowlist`
        );
      }
    }

    const infoChannel = interaction.client.channels.cache.get(INFO_CHANNEL);

    if (!infoChannel) {
      return fail(interaction, "Failed to get info channel");
    }

    const role = interaction.guild?.roles.cache.get(ALLOWLISTED_ROLE);

    if (!role) {
      logger.error("failed to get allowlist role");
      return fail(interaction, "Failed to get role");
    }

    const bedrockRole = interaction.guild?.roles.cache.get(BEDROCK_ROLE);

    if (!bedrockRole) {
      logger.error("failed to get bedrock role");
      return fail(interaction, "Failed to get bedrock role");
    }
  
    const roleRes = await user.roles.add(role).catch(() => false);

    if (roleRes === false) {
      return fail(
        interaction,
        `Failed to give role to "${userId}"`,
        "Failed to give role"
      );
    }

    if (useBedrock) {
      const result = await user.roles.add(bedrockRole).catch(() => false);

      if (result === false) {
        return fail(
          interaction,
          `Failed to give BEDROCK role to "${userId}"`,
          "Failed to give BEDROCK role"
        );
      }
    }

    const userRes = await user
      .send(
        `You've been added to the UoM Minecraft Society allow list! Find out how to join in ${infoChannel}`
      )
      .catch(() => false);

    if (userRes === false) {
      return fail(
        interaction,
        `Failed to send confirmation to "${userId}"`,
        "Failed to send confirmation"
      );
    }

    await interaction
      .update({
        content: clean(interaction) + "```diff\n+ Accepted```",
        components: [],
      })
      .catch(() => logger.error("Failed to mark as accepted"));
  } else {
    const rejectReasonSelect = new StringSelectMenuBuilder()
      .setCustomId("reject-reason")
      .setPlaceholder("Select rejection reason")
      .addOptions(
        new StringSelectMenuOptionBuilder()
          .setLabel(RejectReasons["not-registered"])
          .setValue("not-registered"),
        new StringSelectMenuOptionBuilder()
          .setLabel(RejectReasons["it-username"])
          .setValue("it-username"),
        new StringSelectMenuOptionBuilder()
          .setLabel(RejectReasons["mc-username"])
          .setValue("mc-username"),
        new StringSelectMenuOptionBuilder()
          .setLabel(RejectReasons["email"])
          .setValue("email"),
        new StringSelectMenuOptionBuilder()
          .setLabel(RejectReasons["other"])
          .setValue("other")
      );

    const actionRow = new ActionRowBuilder().addComponents(rejectReasonSelect);

    await interaction
      // @ts-expect-error Cannot type actionRow properly with JSDoc. See https://gitlab.com/uomc/uomc-allowlist-bot/-/issues/5
      .update({ content: clean(interaction), components: [actionRow] })
      .catch(() => logger.error("Failed to add rejection select menu"));
  }
}
