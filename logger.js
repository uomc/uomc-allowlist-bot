import pino from "pino";

export const logger = pino(
  pino.transport({
    targets: [
      { target: "pino/file", level: "info", options: { destination: "logs" } },
      { target: "pino-pretty", level: "warn" }, // STDOUT
    ],
  })
);

/**
 * Logs an error and adds to triggering Discord message
 * @param {import("discord.js").MessageComponentInteraction} interaction
 * @param {string} logMsg Added to error logs
 * @param {string} [discordMsg] Appended to Discord message. Defaults to {@link logMsg}
 */
export function fail(interaction, logMsg, discordMsg) {
  logger.error(logMsg);
  const formattedDiscordMsg = `\n**${discordMsg ?? logMsg}**`;
  interaction.update({
    content: clean(interaction) + formattedDiscordMsg,
  });
}

/**
 * Returns message with errors removed by deleting all characters following `\n**`
 * @param {import("discord.js").MessageComponentInteraction} interaction
 * @returns {string}
 */
export function clean(interaction) {
  return interaction.message.content.replace(/\n\*\*[\w\s*]*/, "");
}
