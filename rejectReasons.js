/** @enum {string} */
export const RejectReasons = {
  "not-registered": "Not registered on SU",
  "it-username": "Used IT username instead of numerical student ID",
  "mc-username": "Invalid Minecraft username",
  email: "Non manchester.ac.uk email",
  other: "Other rejection reason",
};
