# UOMC Allowlist Bot

Discord bot to add members to Minecraft allow list

## Run

1. Install `npm i`
2. Populate `.env`
3. Run: `npm start`

## Commands

- `/allowlist with-id`
  - Minecraft username
  - Student or Student's Union Associate Member ID
- `/allowlist with-email`
  - Minecraft username
  - manchester.ac.uk email address
